﻿namespace ASLE
{
    using Microsoft.Win32;
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Windows;
    using System.Windows.Interop;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    internal class MainView : System.Windows.Forms.IWin32Window
    {
        public Profile Sarge { get; set; }
        public Profile Wildcat { get; set; }
        public Profile Faith { get; set; }
        public Profile Crash { get; set; }
        public Profile Jaeger { get; set; }
        public Profile Wolfe { get; set; }
        public Profile Bastille { get; set; }
        public Profile Vegas { get; set; }

        public Weapons Weapons { get; set; }
        public Extras Extras { get; set; }

        public bool ReactiveDrop
        {
            get
            {
                return Properties.Settings.Default.ReactiveDrop;
            }
            set
            {
                Properties.Settings.Default.ReactiveDrop = value;
                this.OnRestart();
            }
        }

        public bool Portraits2004
        {
            get
            {
                return Properties.Settings.Default.Portraits2004;
            }
            set
            {
                Properties.Settings.Default.Portraits2004 = value;
                Properties.Settings.Default.Save();
                this.OnChangeProfilePictures();
            }
        }

        public bool DisableRestrictions
        {
            get
            {
                return Properties.Settings.Default.DisableClassRestrictions;
            }
            set
            {
                Properties.Settings.Default.DisableClassRestrictions = value;
                this.OnRestart();
            }
        }

        public bool ShowHidden
        {
            get
            {
                return Properties.Settings.Default.ShowHidden;
            }
            set
            {
                Properties.Settings.Default.ShowHidden = value;
                this.OnRestart();
            }
        }

        public int UnlockLevel
        {
            get
            {
                return Properties.Settings.Default.UnlockLevel;
            }
            set
            {
                Properties.Settings.Default.UnlockLevel = value;
                this.OnRestart();
            }
        }

        public int MaxLevel
        {
            get
            {
                return this.ReactiveDrop ? 34 : 27;
            }
        }

        public ImageSource WindowIcon
        {
            get
            {
                return this.ReactiveDrop ? LoadImage("reactivedrop.ico") : LoadImage("swarm.ico"); ;
            }
        }

        public string WindowTitle
        {
            get
            {
                return this.ReactiveDrop ? "Alien Swarm: Reactive Drop Loadout Editor" : "Alien Swarm Loadout Editor";
            }
        }

        public IntPtr Handle
        {
            get
            {
                if (Application.Current.MainWindow != null)
                    return new WindowInteropHelper(Application.Current.MainWindow).Handle;
                else
                    return IntPtr.Zero;
            }
        }

        internal static bool Restart { get; private set; }

        private static BitmapSource _LockedImage;
        internal static BitmapSource LockedImage
        {
            get
            {
                if (_LockedImage == null)
                    _LockedImage = LoadImage("Images.lockedwide.png");

                return _LockedImage;
            }
        }

        //an override save location
        private string AppConfigDirectory
        {
            get
            {
                return Properties.Settings.Default.AppConfigDirectory;
            }
        }

        private string _SteamDirectory;
        private string SteamDirectory
        {
            get
            {
                //use a variable for steam dir so PathNotFound will not be saved
                _SteamDirectory = Properties.Settings.Default.SteamDirectory;

                if (string.IsNullOrWhiteSpace(_SteamDirectory))
                {
                    var dir = string.Empty;
                    var key = Registry.CurrentUser.OpenSubKey(@"Software\Valve\Steam");
                    if (key != null)
                    {
                        dir = key.GetValue("SteamPath") as string;
                        key.Dispose();
                    }

                    if (!string.IsNullOrWhiteSpace(dir))
                    {
                        _SteamDirectory = new DirectoryInfo(dir).FullName;
                    }
                    else
                    {
                        using (var fbd = new System.Windows.Forms.FolderBrowserDialog())
                        {
                            var game = this.ReactiveDrop ? "Alien Swarm: Reactive Drop" : "Alien Swarm";
                            fbd.Description = string.Format("Select the Steam directory {0} is installed in.", game);
                            if (fbd.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                            {
                                _SteamDirectory = Properties.Settings.Default.SteamDirectory = fbd.SelectedPath;
                            }
                            else
                            {
                                //set to invalid path to avoid null exception
                                _SteamDirectory = "PathNotFound";
                            }
                        }
                    }
                }

                return _SteamDirectory;
            }
        }

        internal MainView()
        {
            this.Initialize();
        }

        internal void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            string message = null;
            var valid = this.Validate(out message);
            e.Cancel = !valid && !Restart;
            if (e.Cancel)
                Message.Show(this, message, this.WindowTitle, MessageBoxButton.OK, MessageBoxImage.Warning);

            if (valid && !Restart) this.Save();
        }

        private void OnRestart()
        {
            if (Application.Current.MainWindow.IsLoaded)
            {
                Properties.Settings.Default.Save();
                Restart = true;
                Application.Current.MainWindow.Close();
            }
        }

        private void OnChangeProfilePictures()
        {
            string ext = this.Portraits2004 ? "2k4.png" : ".png";

            this.Sarge.Image = LoadImage(string.Format("Images.sarge{0}", ext));
            this.Wildcat.Image = LoadImage(string.Format("Images.wildcat{0}", ext));
            this.Faith.Image = LoadImage(string.Format("Images.faith{0}", ext));
            this.Crash.Image = LoadImage(string.Format("Images.crash{0}", ext));
            this.Jaeger.Image = LoadImage(string.Format("Images.jaeger{0}", ext));
            this.Wolfe.Image = LoadImage(string.Format("Images.wolfe{0}", ext));
            this.Bastille.Image = LoadImage(string.Format("Images.bastille{0}", ext));
            this.Vegas.Image = LoadImage(string.Format("Images.vegas{0}", ext));
        }

        private void Save()
        {
            string name = "loadouts.cfg";

            //if override save location is set use it
            if (!string.IsNullOrWhiteSpace(this.AppConfigDirectory))
            {
                if (Directory.Exists(this.AppConfigDirectory))
                {
                    this.Write(Path.Combine(this.AppConfigDirectory, name));
                    this.WriteAutoExec(this.AppConfigDirectory);
                    return;
                }
            }

            string game = string.Format("steamapps\\common\\{0}\\cfg", this.ReactiveDrop ? "Alien Swarm Reactive Drop\\reactivedrop" : "Alien Swarm\\swarm");
            string directory = Path.Combine(SteamDirectory, game);
            string file = Path.Combine(directory, name);

            if (Directory.Exists(directory))
            {
                this.Write(file);
                this.WriteAutoExec(directory);
            }
            else
            {
                //SteamDirectory is PathNotFound
                var sfd = new SaveFileDialog();
                sfd.Filter = "All Files|*.*";
                sfd.FileName = name;
                if (sfd.ShowDialog(Application.Current.MainWindow) == true)
                {
                    this.Write(sfd.FileName);
                }
            }
        }

        private void WriteAutoExec(string directory)
        {
            if (ReactiveDrop) return;

            var file = System.IO.Path.Combine(directory, "autoexec.cfg");
            if (File.Exists(file))
            {
                if (Properties.Settings.Default.AppendAutoExec)
                {
                    if (Message.Show(this, "Append \"autoexec.cfg\" to execute \"loadouts.cfg\"?", this.WindowTitle, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        try
                        {
                            File.AppendAllText(file, "\nexec loadouts.cfg\n");
                            Properties.Settings.Default.AppendAutoExec = false;
                            Properties.Settings.Default.Save();
                        }
                        catch (Exception ex)
                        {
                            Message.Show(this, ex.Message, this.WindowTitle, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }
            else
            {
                try
                {
                    File.WriteAllText(file, "exec loadouts.cfg\n");
                }
                catch (Exception ex)
                {
                    Message.Show(this, ex.Message, this.WindowTitle, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private bool Validate(out string message)
        {
            message = string.Empty;

            bool returnValue = true;
            string tmpMessage = string.Empty;

            if (!IsValid(this.Sarge, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }
            if (!IsValid(this.Wildcat, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }
            if (!IsValid(this.Faith, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }
            if (!IsValid(this.Crash, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }
            if (!IsValid(this.Jaeger, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }
            if (!IsValid(this.Wolfe, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }
            if (!IsValid(this.Bastille, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }
            if (!IsValid(this.Vegas, out tmpMessage))
            {
                returnValue = false;
                message += tmpMessage;
            }

            return returnValue;
        }

        private void Initialize()
        {
            this.Weapons = new Weapons();

            this.Weapons.Add(new Weapon(1, "22A3-1 Assault Rifle", Classes.None, LoadImage("Images.rifle.png")));
            this.Weapons.Add(new Weapon(1, "22A7-Z Prototype Assault Rifle", Classes.Tech, LoadImage("Images.prifle.png")));
            this.Weapons.Add(new Weapon(1, "S23A SynTek Autogun", Classes.SpecialWeapons, LoadImage("Images.autogun.png")));
            this.Weapons.Add(new Weapon(1, "M42 Vindicator", Classes.Officer, LoadImage("Images.shotgunassault.png")));
            this.Weapons.Add(new Weapon(1, "M73 Twin Pistols", Classes.None, LoadImage("Images.pistol.png")));
            this.Weapons.Add(new Weapon(1, "IAF Advanced Sentry Gun", Classes.None, LoadImage("Images.sentry.png")));
            this.Weapons.Add(new Weapon(1, "IAF Heal Beacon", Classes.Medic, LoadImage("Images.healgrenade.png")));
            this.Weapons.Add(new Weapon(1, "IAF Ammo Satchel", Classes.None, LoadImage("Images.ammosatchel.png")));

            //Unlocks
            this.Weapons.Add(new Weapon(3, "Model 35 Pump-action Shotgun", Classes.None, LoadImage("Images.shotgun.png")));
            this.Weapons.Add(new Weapon(5, "IAF Tesla Cannon", Classes.None, LoadImage("Images.teslagun.png")));
            this.Weapons.Add(new Weapon(7, "Precision Rail Rifle", Classes.None, LoadImage("Images.railgun.png")));
            this.Weapons.Add(new Weapon(9, "IAF Medical Gun", Classes.Medic, LoadImage("Images.healgun.png")));
            this.Weapons.Add(new Weapon(11, "K80 Personal Defense Weapon", Classes.None, LoadImage("Images.pdw.png")));
            this.Weapons.Add(new Weapon(13, "M868 Flamer Unit", Classes.None, LoadImage("Images.flamer.png")));
            this.Weapons.Add(new Weapon(15, "IAF Freeze Sentry Gun", Classes.None, LoadImage("Images.sentryfreeze.png")));
            this.Weapons.Add(new Weapon(17, "IAF Minigun", Classes.SpecialWeapons, LoadImage("Images.minigun.png")));
            this.Weapons.Add(new Weapon(19, "AVK-36 Marksman Rifle", Classes.None, LoadImage("Images.sniperrifle.png")));
            this.Weapons.Add(new Weapon(21, "IAF Incendiary Sentry Gun", Classes.None, LoadImage("Images.sentryflamer.png")));
            this.Weapons.Add(new Weapon(23, "Chainsaw", Classes.None, LoadImage("Images.chainsaw.png")));
            this.Weapons.Add(new Weapon(25, "IAF High Velocity Sentry Cannon", Classes.None, LoadImage("Images.sentrycannon.png")));
            this.Weapons.Add(new Weapon(27, "Grenade Launcher", Classes.None, LoadImage("Images.grenadelauncher.png")));

            if (this.ReactiveDrop)
            {
                this.Weapons.Add(new Weapon(28, "Desert Eagle", Classes.None, LoadImage("Images.deagle.png")));
                this.Weapons.Add(new Weapon(29, "IAF HAS42 Devastator", Classes.SpecialWeapons, LoadImage("Images.devastator.png")));
                this.Weapons.Add(new Weapon(30, "22A4-2 Combat Rifle", Classes.None, LoadImage("Images.combatrifle.png")));
                this.Weapons.Add(new Weapon(31, "IAF Medical Amplifier Gun", Classes.Medic, LoadImage("Images.healgun.png")));
                this.Weapons.Add(new Weapon(33, "22A5 Heavy Assault Rifle", Classes.None, LoadImage("Images.heavyrifle.png")));
                this.Weapons.Add(new Weapon(34, "IAF Medical SMG", Classes.Medic, LoadImage("Images.medrifle.png")));
            }

            //Hidden
            if (this.ShowHidden)
            {
                this.Weapons.Add(new Weapon(1, "Fire Extinguisher", Classes.None, LoadImage("Images.fireext.png")));
                this.Weapons.Add(new Weapon(1, "Mining Laser", Classes.None, LoadImage("Images.mininglaser.png")));

                if (Properties.Settings.Default.ReactiveDrop)
                    this.Weapons.Add(new Weapon(1, "50CMG4-1", Classes.None, LoadImage("Images.50calmg.png")));

                this.Weapons.Add(new Weapon(1, "Ricochet Rifle", Classes.None, LoadImage("Images.rifle.png")));
                this.Weapons.Add(new Weapon(1, "Flechette Launcher", Classes.None, LoadImage("Images.grenadelauncher.png")));
                this.Weapons.Add(new Weapon(1, "IAF Ammo Bag", Classes.None, LoadImage("Images.ammobag.png")));
                this.Weapons.Add(new Weapon(1, "IAF Medical Satchel", Classes.Medic, LoadImage("Images.medsatchel.png")));
            }

            this.Extras = new Extras();

            this.Extras.Add(new Extra(1, "IAF Personal Healing Kit", Classes.None, LoadImage("Images.medkit.png")));
            this.Extras.Add(new Extra(1, "Hand Welder", Classes.None, LoadImage("Images.welder.png")));
            this.Extras.Add(new Extra(1, "SM75 Combat Flares", Classes.None, LoadImage("Images.flares.png")));
            this.Extras.Add(new Extra(1, "ML30 Laser Trip Mine", Classes.None, LoadImage("Images.lasermines.png")));

            //Unlocks
            this.Extras.Add(new Extra(2, "l3a Tactical Heavy Armor", Classes.None, LoadImage("Images.normalarmor.png")));
            this.Extras.Add(new Extra(4, "X33 Damage Amplifier", Classes.None, LoadImage("Images.buffgrenade.png")));
            this.Extras.Add(new Extra(6, "Hornet Barrage", Classes.None, LoadImage("Images.hornetbarrage.png")));
            this.Extras.Add(new Extra(8, "Freeze Grenades", Classes.None, LoadImage("Images.freezegrenade.png")));
            this.Extras.Add(new Extra(10, "Adrenaline", Classes.None, LoadImage("Images.stims.png")));
            this.Extras.Add(new Extra(12, "IAF Tesla Sentry Coil", Classes.None, LoadImage("Images.teslatrap.png")));
            this.Extras.Add(new Extra(14, "v45 Electric Charged Armor", Classes.None, LoadImage("Images.electrifiedarmor.png")));
            this.Extras.Add(new Extra(16, "M478 Proximity Incendiary Mines", Classes.Officer, LoadImage("Images.mines.png")));
            this.Extras.Add(new Extra(18, "Flashlight Attachment", Classes.None, LoadImage("Images.flashlight.png")));
            this.Extras.Add(new Extra(20, "IAF Power Fist Attachment", Classes.None, LoadImage("Images.powerfist.png")));
            this.Extras.Add(new Extra(22, "Hand Grenades", Classes.None, LoadImage("Images.grenade.png")));
            this.Extras.Add(new Extra(24, "MNV34 Nightvision Goggles", Classes.None, LoadImage("Images.goggles.png")));
            this.Extras.Add(new Extra(26, "MTD6 Smart Bomb", Classes.None, LoadImage("Images.smartbomb.png")));

            if (this.ReactiveDrop)
                this.Extras.Add(new Extra(32, "TG-05 Gas Grenades", Classes.Medic, LoadImage("Images.gasgrenades.png")));

            //Hidden
            if (this.ShowHidden)
            {
                this.Extras.Add(new Extra(1, "Short Range Assault Jets", Classes.None, LoadImage("Images.jumpjet.png")));
                this.Extras.Add(new Extra(1, "Displacement 'Blink' Pack", Classes.None, LoadImage("Images.blink.png")));
                this.Extras.Add(new Extra(1, "T75 Explosives", Classes.Officer, LoadImage("Images.t75.png")));
                this.Extras.Add(new Extra(1, "Swarm Bait", Classes.None, LoadImage("Images.bait.png")));
                //this.Extras.Add(new Extra(1, "Fire Extinguisher", Classes.None, LoadImage("Images.fireext.png")));
            }

            this.Sarge = new Profile(0);
            this.Sarge.PropertyChanged += this.Profile_PropertyChanged;
            this.Sarge.Name = "Sarge";
            this.Sarge.Class = Classes.Officer;
            this.Sarge.WeaponSlot1 = this.Weapons[3];
            this.Sarge.WeaponSlot2 = this.Weapons[0];
            this.Sarge.ExtraSlot = this.Extras[3];

            this.Wildcat = new Profile(1);
            this.Wildcat.PropertyChanged += this.Profile_PropertyChanged;
            this.Wildcat.Name = "Wildcat";
            this.Wildcat.Class = Classes.SpecialWeapons;
            this.Wildcat.WeaponSlot1 = this.Weapons[2];
            this.Wildcat.WeaponSlot2 = this.Weapons[7];
            this.Wildcat.ExtraSlot = this.Extras[0];

            this.Faith = new Profile(2);
            this.Faith.PropertyChanged += this.Profile_PropertyChanged;
            this.Faith.Name = "Faith";
            this.Faith.Class = Classes.Medic;
            this.Faith.WeaponSlot1 = this.Weapons[0];
            this.Faith.WeaponSlot2 = this.Weapons[6];
            this.Faith.ExtraSlot = this.Extras[2];

            this.Crash = new Profile(3);
            this.Crash.PropertyChanged += this.Profile_PropertyChanged;
            this.Crash.Name = "Crash";
            this.Crash.Class = Classes.Tech;
            this.Crash.WeaponSlot1 = this.Weapons[1];
            this.Crash.WeaponSlot2 = this.Weapons[5];
            this.Crash.ExtraSlot = this.Extras[1];

            this.Jaeger = new Profile(4);
            this.Jaeger.PropertyChanged += this.Profile_PropertyChanged;
            this.Jaeger.Name = "Jaeger";
            this.Jaeger.Class = Classes.Officer;
            this.Jaeger.WeaponSlot1 = this.Weapons[3];
            this.Jaeger.WeaponSlot2 = this.Weapons[0];
            this.Jaeger.ExtraSlot = this.Extras[3];

            this.Wolfe = new Profile(5);
            this.Wolfe.PropertyChanged += this.Profile_PropertyChanged;
            this.Wolfe.Name = "Wolfe";
            this.Wolfe.Class = Classes.SpecialWeapons;
            this.Wolfe.WeaponSlot1 = this.Weapons[2];
            this.Wolfe.WeaponSlot2 = this.Weapons[7];
            this.Wolfe.ExtraSlot = this.Extras[0];

            this.Bastille = new Profile(6);
            this.Bastille.PropertyChanged += this.Profile_PropertyChanged;
            this.Bastille.Name = "Bastille";
            this.Bastille.Class = Classes.Medic;
            this.Bastille.WeaponSlot1 = this.Weapons[0];
            this.Bastille.WeaponSlot2 = this.Weapons[6];
            this.Bastille.ExtraSlot = this.Extras[2];

            this.Vegas = new Profile(7);
            this.Vegas.PropertyChanged += this.Profile_PropertyChanged;
            this.Vegas.Name = "Vegas";
            this.Vegas.Class = Classes.Tech;
            this.Vegas.WeaponSlot1 = this.Weapons[1];
            this.Vegas.WeaponSlot2 = this.Weapons[5];
            this.Vegas.ExtraSlot = this.Extras[1];

            this.OnChangeProfilePictures();
            this.LoadConfig();
        }

        private void Profile_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "WeaponSlot1":
                case "WeaponSlot2":
                case "ExtraSlot":
                    var p = sender as Profile;
                    if (p != null)
                    {
                        string message;
                        IsValid(p, out message);
                    }
                    break;
            }
        }

        private void LoadConfig()
        {
            string name = "loadouts.cfg";

            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.AppConfigDirectory))
            {
                if (Directory.Exists(Properties.Settings.Default.AppConfigDirectory))
                {
                    this.Parse(Path.Combine(Properties.Settings.Default.AppConfigDirectory, name));
                    return;
                }
            }

            string game = string.Format("steamapps\\common\\{0}\\cfg", this.ReactiveDrop ? "Alien Swarm Reactive Drop\\reactivedrop" : "Alien Swarm\\swarm");
            string directory = Path.Combine(SteamDirectory, game);
            string file = Path.Combine(directory, name);

            if (Directory.Exists(directory))
            {
                this.Parse(file);
            }
        }

        private void Parse(string file)
        {
            if (File.Exists(file))
            {
                try
                {
                    var lines = File.ReadAllLines(file);
                    foreach (var line in lines)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            var test = line.Trim().ToLowerInvariant();
                            foreach (var command in Profile.Commands)
                            {
                                if (test.StartsWith(command))
                                {
                                    var words = test.Split(new[] { ' ' });
                                    if (words.Length > 1 && !string.IsNullOrWhiteSpace(words[1]))
                                    {
                                        int i = 0;
                                        if (int.TryParse(words[1], out i))
                                        {
                                            switch (command)
                                            {
                                                case Profile.SargeWeapon1:
                                                    this.SetWeapon(this.Sarge, 1, i);
                                                    break;
                                                case Profile.SargeWeapon2:
                                                    this.SetWeapon(this.Sarge, 2, i);
                                                    break;
                                                case Profile.SargeExtra:
                                                    this.SetExtra(this.Sarge, i);
                                                    break;

                                                case Profile.WildcatWeapon1:
                                                    this.SetWeapon(this.Wildcat, 1, i);
                                                    break;
                                                case Profile.WildcatWeapon2:
                                                    this.SetWeapon(this.Wildcat, 2, i);
                                                    break;
                                                case Profile.WildcatExtra:
                                                    this.SetExtra(this.Wildcat, i);
                                                    break;

                                                case Profile.FaithWeapon1:
                                                    this.SetWeapon(this.Faith, 1, i);
                                                    break;
                                                case Profile.FaithWeapon2:
                                                    this.SetWeapon(this.Faith, 2, i);
                                                    break;
                                                case Profile.FaithExtra:
                                                    this.SetExtra(this.Faith, i);
                                                    break;

                                                case Profile.CrashWeapon1:
                                                    this.SetWeapon(this.Crash, 1, i);
                                                    break;
                                                case Profile.CrashWeapon2:
                                                    this.SetWeapon(this.Crash, 2, i);
                                                    break;
                                                case Profile.CrashExtra:
                                                    this.SetExtra(this.Crash, i);
                                                    break;

                                                case Profile.JaegerWeapon1:
                                                    this.SetWeapon(this.Jaeger, 1, i);
                                                    break;
                                                case Profile.JaegerWeapon2:
                                                    this.SetWeapon(this.Jaeger, 2, i);
                                                    break;
                                                case Profile.JaegerExtra:
                                                    this.SetExtra(this.Jaeger, i);
                                                    break;

                                                case Profile.WolfeWeapon1:
                                                    this.SetWeapon(this.Wolfe, 1, i);
                                                    break;
                                                case Profile.WolfeWeapon2:
                                                    this.SetWeapon(this.Wolfe, 2, i);
                                                    break;
                                                case Profile.WolfeExtra:
                                                    this.SetExtra(this.Wolfe, i);
                                                    break;

                                                case Profile.BastilleWeapon1:
                                                    this.SetWeapon(this.Bastille, 1, i);
                                                    break;
                                                case Profile.BastilleWeapon2:
                                                    this.SetWeapon(this.Bastille, 2, i);
                                                    break;
                                                case Profile.BastilleExtra:
                                                    this.SetExtra(this.Bastille, i);
                                                    break;

                                                case Profile.VegasWeapon1:
                                                    this.SetWeapon(this.Vegas, 1, i);
                                                    break;
                                                case Profile.VegasWeapon2:
                                                    this.SetWeapon(this.Vegas, 2, i);
                                                    break;
                                                case Profile.VegasExtra:
                                                    this.SetExtra(this.Vegas, i);
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Message.Show(this, ex.Message, this.WindowTitle, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void SetWeapon(Profile p, int slot, int index)
        {
            if (index > -1 && index < this.Weapons.Count)
            {
                if (slot == 1)
                    p.WeaponSlot1 = this.Weapons[index];
                else
                    p.WeaponSlot2 = this.Weapons[index];
            }
        }

        private void SetExtra(Profile p, int index)
        {
            if (index > -1 && index < this.Extras.Count)
            {
                p.ExtraSlot = this.Extras[index];
            }
        }

        private void Write(string file)
        {
            var sb = new StringBuilder();
            sb.Append("//---------------------------------------------------------------\n//     This code was generated by a tool.\n//     Changes to this file may cause incorrect\n//     behavior and will be lost if the code is regenerated.\n//---------------------------------------------------------------\n\n");
            sb.Append(this.Read(this.Sarge));
            sb.Append(this.Read(this.Wildcat));
            sb.Append(this.Read(this.Faith));
            sb.Append(this.Read(this.Crash));
            sb.Append(this.Read(this.Jaeger));
            sb.Append(this.Read(this.Wolfe));
            sb.Append(this.Read(this.Bastille));
            sb.Append(this.Read(this.Vegas));

            try
            {
                File.WriteAllText(file, sb.ToString());
            }
            catch (Exception ex)
            {
                Message.Show(this, ex.Message, this.WindowTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private string Read(Profile p)
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("//{0}", p.Name));
            sb.AppendLine(string.Format("asw_default_primary_{0} {1}", p.Index, this.Weapons.IndexOf(p.WeaponSlot1)));
            sb.AppendLine(string.Format("asw_default_secondary_{0} {1}", p.Index, this.Weapons.IndexOf(p.WeaponSlot2)));
            sb.AppendLine(string.Format("asw_default_extra_{0} {1}", p.Index, this.Extras.IndexOf(p.ExtraSlot)));
            sb.AppendLine();

            return sb.ToString();
        }

        private static bool IsValid(Profile p, out string message)
        {
            bool returnValue = true;
            message = string.Empty;

            if (p.WeaponSlot1 != null && p.WeaponSlot2 != null && p.ExtraSlot != null)
            {
                p.InvalidWeapon1 = false;
                p.InvalidWeapon2 = false;
                p.InvalidExtra = false;

                if (!Properties.Settings.Default.DisableClassRestrictions)
                {
                    if (p.Class != p.WeaponSlot1.Class && p.WeaponSlot1.Class != Classes.None)
                    {
                        p.InvalidWeapon1 = true;
                        message += string.Format("Invalid class for {0}'s selected primary weapon.\n", p.Name);
                        returnValue = false;
                    }
                    if (p.Class != p.WeaponSlot2.Class && p.WeaponSlot2.Class != Classes.None)
                    {
                        p.InvalidWeapon2 = true;
                        message += string.Format("Invalid class for {0}'s selected secondary weapon.\n", p.Name);
                        returnValue = false;
                    }

                    if (p.Class != p.ExtraSlot.Class && p.ExtraSlot.Class != Classes.None)
                    {
                        p.InvalidExtra = true;
                        message += string.Format("Invalid class for {0}'s selected extra item.\n", p.Name);
                        returnValue = false;
                    }
                }

                if (Properties.Settings.Default.UnlockLevel < p.WeaponSlot1.UnlockLevel)
                {
                    //p.InvalidWeapon1 = true;
                    message += string.Format("Insufficient level for {0}'s selected primary weapon.\n", p.Name);
                    returnValue = false;
                }

                if (Properties.Settings.Default.UnlockLevel < p.WeaponSlot2.UnlockLevel)
                {
                    //p.InvalidWeapon2 = true;
                    message += string.Format("Insufficient level for {0}'s selected secondary weapon.\n", p.Name);
                    returnValue = false;
                }

                if (Properties.Settings.Default.UnlockLevel < p.ExtraSlot.UnlockLevel)
                {
                    //p.InvalidExtra = true;
                    message += string.Format("Insufficient level for {0}'s selected extra item.\n", p.Name);
                    returnValue = false;
                }
            }

            return returnValue;
        }

        private static BitmapSource LoadImage(string name)
        {
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Format("ASLE.{0}", name));
            stream.Seek(0, SeekOrigin.Begin);
            var image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();

            return image;
        }
    }
}
