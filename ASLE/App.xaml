﻿<Application x:Class="ASLE.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             ShutdownMode="OnMainWindowClose"
             DispatcherUnhandledException="Application_DispatcherUnhandledException"
             Startup="Application_Startup"
             Exit="Application_Exit">
    <Application.Resources>
        <ResourceDictionary>
            <!-- NormalBrush is used as the Background for SimpleButton, SimpleRepeatButton -->
            <LinearGradientBrush x:Key="NormalBrush" EndPoint="0,1" StartPoint="0,0">
                <GradientStop Color="#EEE" Offset="0.0"/>
                <GradientStop Color="#CCC" Offset="1.0"/>
            </LinearGradientBrush>
            <LinearGradientBrush x:Key="NormalBorderBrush" EndPoint="0,1" StartPoint="0,0">
                <GradientStop Color="#CCC" Offset="0.0"/>
                <GradientStop Color="#444" Offset="1.0"/>
            </LinearGradientBrush>

            <!-- LightBrush is used for content areas such as Menu, Tab Control background -->
            <LinearGradientBrush x:Key="LightBrush" EndPoint="0,1" StartPoint="0,0">
                <GradientStop Color="#FFF" Offset="0.0"/>
                <GradientStop Color="#EEE" Offset="1.0"/>
            </LinearGradientBrush>

            <!-- MouseOverBrush is used for MouseOver in Button, Radio Button, CheckBox -->
            <LinearGradientBrush x:Key="MouseOverBrush" EndPoint="0,1" StartPoint="0,0">
                <GradientStop Color="WhiteSmoke" Offset="0.0"/>
                <GradientStop Color="Gainsboro" Offset="1.0"/>
            </LinearGradientBrush>

            <!-- PressedBrush is used for Pressed in Button, Radio Button, CheckBox -->
            <LinearGradientBrush x:Key="PressedBrush" EndPoint="0,1" StartPoint="0,0">
                <GradientStop Color="#BBB" Offset="0.0"/>
                <GradientStop Color="#EEE" Offset="0.1"/>
                <GradientStop Color="#EEE" Offset="0.9"/>
                <GradientStop Color="#FFF" Offset="1.0"/>
            </LinearGradientBrush>
            <LinearGradientBrush x:Key="PressedBorderBrush" EndPoint="0,1" StartPoint="0,0">
                <GradientStop Color="#444" Offset="0.0"/>
                <GradientStop Color="#888" Offset="1.0"/>
            </LinearGradientBrush>

            <!-- SelectedBackgroundBrush is used for the Selected item in ListBoxItem, ComboBoxItem-->
            <SolidColorBrush x:Key="SelectedBackgroundBrush" Color="#DDD"/>

            <!-- Disabled Brushes are used for the Disabled look of each control -->
            <SolidColorBrush x:Key="DisabledForegroundBrush" Color="#888"/>
            <SolidColorBrush x:Key="DisabledBackgroundBrush" Color="#EEE"/>
            <SolidColorBrush x:Key="DisabledBorderBrush" Color="#AAA"/>

            <LinearGradientBrush x:Key="DefaultedBorderBrush" Opacity="0.05" EndPoint="0,1" StartPoint="0,0">
                <GradientStop Color="#FFF" Offset="0.0"/>
                <GradientStop Color="#AAA" Offset="1.0"/>
            </LinearGradientBrush>
            
            <SolidColorBrush x:Key="SolidBorderBrush" Color="#888"/>
            <SolidColorBrush x:Key="LightBorderBrush" Color="#AAA"/>
            <SolidColorBrush x:Key="LightColorBrush" Color="#DDD"/>
            
            <!-- Used for background of ScrollViewer, TreeView, ListBox, Expander, TextBox, Tab Control -->
            <!--<SolidColorBrush x:Key="WindowBackgroundBrush" Color="Black"/>-->
            <ImageBrush x:Key="WindowBackgroundBrush" ImageSource="Images/lobby_row_bg.png" />

            <!-- Simple ComboBox Toggle Button - This is used in ComboBox to expand and collapse the ComboBox Popup-->
            <ControlTemplate x:Key="ComboBoxToggleButton" TargetType="{x:Type ToggleButton}">
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition/>
                        <ColumnDefinition Width="20"/>
                    </Grid.ColumnDefinitions>
                    <Rectangle Grid.ColumnSpan="2" HorizontalAlignment="Stretch" x:Name="Rectangle" VerticalAlignment="Stretch" Width="Auto" Height="Auto" RadiusX="5" RadiusY="5" Fill="{DynamicResource DefaultedBorderBrush}" Stroke="{x:Null}" Opacity="0.5"/>
                    <Rectangle Grid.Column="0" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" Width="Auto" Height="Auto" RadiusX="5" RadiusY="5" Fill="{x:Null}" Stroke="{x:Null}"/>
                    <Path Grid.Column="1" HorizontalAlignment="Center" x:Name="Arrow" VerticalAlignment="Center" Fill="{DynamicResource DisabledBackgroundBrush}" Data="M 0 0 L 4 4 L 8 0 Z"/>
                </Grid>
                <ControlTemplate.Triggers>
                    <Trigger Property="IsMouseOver" Value="True">
                        <Setter Property="Fill" Value="{DynamicResource MouseOverBrush}" TargetName="Rectangle"/>
                    </Trigger>
                    <Trigger Property="IsChecked" Value="True">
                        <Setter Property="Fill" Value="{DynamicResource PressedBrush}" TargetName="Rectangle"/>
                    </Trigger>
                    <Trigger Property="IsEnabled" Value="False">
                        <Setter Property="Fill" Value="{DynamicResource DisabledBackgroundBrush}" TargetName="Rectangle"/>
                        <Setter Property="Stroke" Value="{DynamicResource DisabledBorderBrush}" TargetName="Rectangle"/>
                        <Setter Property="Foreground" Value="{DynamicResource DisabledForegroundBrush}"/>
                        <Setter Property="Fill" Value="{DynamicResource DisabledForegroundBrush}" TargetName="Arrow"/>
                    </Trigger>
                </ControlTemplate.Triggers>
            </ControlTemplate>

            <!-- This is the area which contains the selected item in the ComboBox -->
            <ControlTemplate x:Key="ComboBoxTextBox" TargetType="{x:Type TextBox}">
                <!-- This must be named as PART_ContentHost -->
                <Border x:Name="PART_ContentHost" Focusable="False" Background="{TemplateBinding Background}"/>
            </ControlTemplate>

            <!-- Simple ComboBox 
	             This uses the ComboBoxToggleButton to expand and collapse a Popup control
	             SimpleScrollViewer to allow items to be scrolled and SimpleComboBoxItem to define the look of each item 
	             The Popup shows a list of items in a StackPanel-->
            <Style x:Key="SimpleComboBox" TargetType="{x:Type ComboBox}">
                <Setter Property="SnapsToDevicePixels" Value="True"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type ComboBox}">
                            <Grid>
                                <!-- The ToggleButton is databound to the ComboBox itself to toggle IsDropDownOpen -->
                                <ToggleButton Grid.Column="2" Template="{DynamicResource ComboBoxToggleButton}" x:Name="ToggleButton" Focusable="False" IsChecked="{Binding Path=IsDropDownOpen, Mode=TwoWay, RelativeSource={RelativeSource TemplatedParent}}" ClickMode="Press"/>
                                <ContentPresenter Margin="3,3,23,3" x:Name="ContentSite" VerticalAlignment="Center" Content="{TemplateBinding SelectionBoxItem}" ContentTemplate="{TemplateBinding SelectionBoxItemTemplate}" ContentTemplateSelector="{TemplateBinding ItemTemplateSelector}" IsHitTestVisible="False"/>

                                <!-- The TextBox must be named PART_EditableTextBox or ComboBox will not recognize it -->
                                <TextBox Visibility="Hidden" Template="{DynamicResource ComboBoxTextBox}" HorizontalAlignment="Left" Margin="3,3,23,3" x:Name="PART_EditableTextBox" Style="{x:Null}" VerticalAlignment="Center" Focusable="True" Background="Transparent" IsReadOnly="{TemplateBinding IsReadOnly}"/>

                                <!-- The Popup shows the list of items in the ComboBox. IsOpen is databound to IsDropDownOpen which is toggled via the ComboBoxToggleButton -->
                                <Popup IsOpen="{TemplateBinding IsDropDownOpen}" Placement="Bottom" x:Name="Popup" Focusable="False" AllowsTransparency="True" PopupAnimation="Slide">
                                    <Grid MaxHeight="{TemplateBinding MaxDropDownHeight}" MinWidth="{TemplateBinding ActualWidth}" x:Name="DropDown" SnapsToDevicePixels="True">
                                        <Border x:Name="DropDownBorder" Background="{DynamicResource WindowBackgroundBrush}" BorderBrush="{DynamicResource SolidBorderBrush}" BorderThickness="1" Opacity="0.95"/>
                                        <ScrollViewer Margin="4,6,4,6" Style="{DynamicResource SimpleScrollViewer}" SnapsToDevicePixels="True" HorizontalScrollBarVisibility="Auto" VerticalScrollBarVisibility="Auto" CanContentScroll="True">
                                            <!-- The StackPanel is used to display the children by setting IsItemsHost to be True -->
                                            <StackPanel IsItemsHost="True" KeyboardNavigation.DirectionalNavigation="Contained"/>
                                        </ScrollViewer>
                                    </Grid>
                                </Popup>
                            </Grid>
                            <ControlTemplate.Triggers>
                                <!-- This forces the DropDown to have a minimum size if it is empty -->
                                <Trigger Property="HasItems" Value="False">
                                    <Setter Property="MinHeight" Value="95" TargetName="DropDownBorder"/>
                                </Trigger>
                                <Trigger Property="IsEnabled" Value="False">
                                    <Setter Property="Foreground" Value="{DynamicResource DisabledForegroundBrush}"/>
                                </Trigger>
                                <Trigger Property="IsGrouping" Value="True">
                                    <Setter Property="ScrollViewer.CanContentScroll" Value="False"/>
                                </Trigger>
                                <Trigger Property="AllowsTransparency" SourceName="Popup" Value="True">
                                    <Setter Property="CornerRadius" Value="4" TargetName="DropDownBorder"/>
                                    <Setter Property="Margin" Value="0,2,0,0" TargetName="DropDownBorder"/>
                                </Trigger>
                                <Trigger Property="IsEditable" Value="True">
                                    <Setter Property="IsTabStop" Value="False"/>
                                    <Setter Property="Visibility" Value="Visible" TargetName="PART_EditableTextBox"/>
                                    <Setter Property="Visibility" Value="Hidden" TargetName="ContentSite"/>
                                </Trigger>
                            </ControlTemplate.Triggers>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <!-- Simple ComboBoxItem - This is used for each item inside of the ComboBox. You can change the selected color of each item below-->
            <Style x:Key="SimpleComboBoxItem" TargetType="{x:Type ComboBoxItem}">
                <Setter Property="SnapsToDevicePixels" Value="True"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type ComboBoxItem}">
                            <Grid SnapsToDevicePixels="True">
                                <Border x:Name="Border" Background="{TemplateBinding Background}" BorderBrush="{TemplateBinding BorderBrush}" BorderThickness="{TemplateBinding BorderThickness}"/>
                                <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
                            </Grid>
                            <ControlTemplate.Triggers>
                                <!-- Change IsHighlighted SelectedBackgroundBrush to set the selection color for the items -->
                                <Trigger Property="IsHighlighted" Value="True">
                                    <Setter Property="Background" Value="{DynamicResource SelectedBackgroundBrush}" TargetName="Border"/>
                                </Trigger>
                                <Trigger Property="IsEnabled" Value="False">
                                    <Setter Property="Foreground" Value="{DynamicResource DisabledForegroundBrush}"/>
                                </Trigger>
                            </ControlTemplate.Triggers>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <!-- Simple Repeat Button - This is used by Simple ScrollBar for the up and down buttons -->
            <Style x:Key="SimpleRepeatButton" TargetType="{x:Type RepeatButton}" BasedOn="{x:Null}">
                <Setter Property="Background" Value="{DynamicResource NormalBrush}"/>
                <Setter Property="BorderBrush" Value="{DynamicResource NormalBorderBrush}"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type RepeatButton}">
                            <Grid>
                                <Border x:Name="Border" Background="{TemplateBinding Background}" BorderBrush="{TemplateBinding BorderBrush}" BorderThickness="{TemplateBinding BorderThickness}"/>
                                <ContentPresenter HorizontalAlignment="Center" x:Name="ContentPresenter" VerticalAlignment="Center" Content="{TemplateBinding Content}" ContentTemplate="{TemplateBinding ContentTemplate}" ContentTemplateSelector="{TemplateBinding ContentTemplateSelector}"/>
                            </Grid>
                            <ControlTemplate.Triggers>
                                <Trigger Property="IsKeyboardFocused" Value="True">
                                    <Setter Property="BorderBrush" Value="{DynamicResource DefaultedBorderBrush}" TargetName="Border"/>
                                </Trigger>
                                <Trigger Property="IsMouseOver" Value="True">
                                    <Setter Property="Background" Value="{DynamicResource MouseOverBrush}" TargetName="Border"/>
                                </Trigger>
                                <Trigger Property="IsPressed" Value="True">
                                    <Setter Property="Background" Value="{DynamicResource PressedBrush}" TargetName="Border"/>
                                    <Setter Property="BorderBrush" Value="{DynamicResource PressedBorderBrush}" TargetName="Border"/>
                                </Trigger>
                                <Trigger Property="IsEnabled" Value="False">
                                    <Setter Property="Background" Value="{DynamicResource DisabledBackgroundBrush}" TargetName="Border"/>
                                    <Setter Property="BorderBrush" Value="{DynamicResource DisabledBorderBrush}" TargetName="Border"/>
                                    <Setter Property="Foreground" Value="{DynamicResource DisabledForegroundBrush}"/>
                                </Trigger>
                            </ControlTemplate.Triggers>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <!-- Simple Thumb - The Thumb is the draggable part of the Scrollbar -->
            <Style x:Key="SimpleThumbStyle" TargetType="{x:Type Thumb}" BasedOn="{x:Null}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type Thumb}">
                            <Grid Margin="0,0,0,0" x:Name="Grid">
                                <Rectangle HorizontalAlignment="Stretch" x:Name="Rectangle" Opacity="0.9" VerticalAlignment="Stretch" Width="Auto" Height="Auto" Fill="{DynamicResource NormalBrush}" Stroke="{DynamicResource NormalBorderBrush}"/>
                            </Grid>
                            <ControlTemplate.Triggers>
                                <Trigger Property="IsFocused" Value="True"/>
                                <Trigger Property="IsMouseOver" Value="True">
                                    <Setter Property="Background" Value="{DynamicResource MouseOverBrush}" TargetName="Grid"/>
                                </Trigger>
                                <Trigger Property="IsEnabled" Value="False"/>
                                <Trigger Property="IsDragging" Value="True"/>
                            </ControlTemplate.Triggers>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <!-- Simple ScrollRepeatButton Style - This RepeatButton is used above and below the Thumb in the Scrollbar. They are set to transparent si that they do not show over the scrollbar -->
            <Style x:Key="SimpleScrollRepeatButtonStyle" TargetType="{x:Type RepeatButton}">
                <Setter Property="Background" Value="Transparent"/>
                <Setter Property="BorderBrush" Value="Transparent"/>
                <Setter Property="IsTabStop" Value="False"/>
                <Setter Property="Focusable" Value="False"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type RepeatButton}">
                            <Grid>
                                <Rectangle StrokeThickness="{TemplateBinding BorderThickness}"/>
                            </Grid>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <!-- Simple ScrollBar  This makes use of SimpleThumb, SimpleRepeatButton and SimpleScrollRepeatButton -->
            <Style x:Key="SimpleScrollBar" TargetType="{x:Type ScrollBar}">
                <Setter Property="Stylus.IsFlicksEnabled" Value="False"/>
                <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type ScrollBar}">
                            <Grid x:Name="GridRoot" Width="7.475" Margin="5">
                                <Grid.RowDefinitions>
                                    <RowDefinition MaxHeight="18" Height="0.144*"/>
                                    <RowDefinition Height="0.712*"/>
                                    <RowDefinition MaxHeight="18" Height="0.144*"/>
                                </Grid.RowDefinitions>
                                <RepeatButton x:Name="DecreaseRepeat" Style="{DynamicResource SimpleRepeatButton}" Command="ScrollBar.LineUpCommand">
                                    <RepeatButton.Background>
                                        <LinearGradientBrush EndPoint="0,1" StartPoint="0,0">
                                            <GradientStop Color="#FF444444" Offset="0"/>
                                            <GradientStop Color="#FFCCCCCC" Offset="1"/>
                                        </LinearGradientBrush>
                                    </RepeatButton.Background>
                                    <Grid/>
                                </RepeatButton>

                                <!-- Track is a special layout container which sizes the thumb and the repeat button which do jump scrolling either side of it -->
                                <Track Grid.Row="1" x:Name="PART_Track" Orientation="Vertical" IsDirectionReversed="True">
                                    <Track.Thumb>
                                        <Thumb Style="{DynamicResource SimpleThumbStyle}"/>
                                    </Track.Thumb>
                                    <Track.IncreaseRepeatButton>
                                        <RepeatButton x:Name="PageUp" Style="{DynamicResource SimpleScrollRepeatButtonStyle}" Command="ScrollBar.PageDownCommand" Background="{x:Null}" BorderBrush="{x:Null}"/>
                                    </Track.IncreaseRepeatButton>
                                    <Track.DecreaseRepeatButton>
                                        <RepeatButton x:Name="PageDown" Style="{DynamicResource SimpleScrollRepeatButtonStyle}" Command="ScrollBar.PageUpCommand" Background="{x:Null}" BorderBrush="{x:Null}"/>
                                    </Track.DecreaseRepeatButton>
                                </Track>
                                <RepeatButton Grid.Row="2" x:Name="IncreaseRepeat" Style="{DynamicResource SimpleRepeatButton}" Command="ScrollBar.LineDownCommand">
                                    <RepeatButton.Background>
                                        <LinearGradientBrush EndPoint="0,1" StartPoint="0,0">
                                            <GradientStop Color="#FFEEEEEE" Offset="0"/>
                                            <GradientStop Color="#FF444444" Offset="1"/>
                                        </LinearGradientBrush>
                                    </RepeatButton.Background>
                                    <Grid/>
                                </RepeatButton>
                            </Grid>

                            <!-- This uses a single template for ScrollBar and rotate it to be Horizontal
					            It also changes the commands so that the it does Left and Right instead of Up and Down Commands -->
                            <ControlTemplate.Triggers>
                                <Trigger Property="Orientation" Value="Horizontal">

                                    <!-- Rotate the ScrollBar from Vertical to Horizontal -->
                                    <Setter Property="LayoutTransform" TargetName="GridRoot">
                                        <Setter.Value>
                                            <RotateTransform Angle="-90"/>
                                        </Setter.Value>
                                    </Setter>

                                    <!-- Track is bound to Orientation internally, so we need to rotate it back to Vertical -->
                                    <Setter TargetName="PART_Track" Property="Orientation" Value="Vertical"/>

                                    <!-- Change the commands to do Horizontal commands -->
                                    <Setter Property="Command" Value="ScrollBar.LineLeftCommand" TargetName="DecreaseRepeat"/>
                                    <Setter Property="Command" Value="ScrollBar.LineRightCommand" TargetName="IncreaseRepeat"/>
                                    <Setter Property="Command" Value="ScrollBar.PageLeftCommand" TargetName="PageDown"/>
                                    <Setter Property="Command" Value="ScrollBar.PageRightCommand" TargetName="PageUp"/>
                                </Trigger>
                            </ControlTemplate.Triggers>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <!-- Simple ScrollViewer 
	            ScrollViewer is a Grid control which has a ContentPresenter and a Horizontal and Vertical ScrollBar 
	            It is used by ListBox, MenuItem, ComboBox, and TreeView -->
            <Style x:Key="SimpleScrollViewer" TargetType="{x:Type ScrollViewer}" BasedOn="{x:Null}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type ScrollViewer}">
                            <Grid Background="{TemplateBinding Background}">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="*"/>
                                    <ColumnDefinition Width="Auto"/>
                                </Grid.ColumnDefinitions>
                                <Grid.RowDefinitions>
                                    <RowDefinition Height="*"/>
                                    <RowDefinition Height="Auto"/>
                                </Grid.RowDefinitions>
                                <ScrollContentPresenter Grid.Column="0" Grid.Row="0" Margin="{TemplateBinding Padding}" Content="{TemplateBinding Content}" ContentTemplate="{TemplateBinding ContentTemplate}" CanContentScroll="{TemplateBinding CanContentScroll}"/>
                                <!-- The visibility of the ScrollBars is controlled by the implementation fo the control -->
                                <ScrollBar Visibility="{TemplateBinding ComputedHorizontalScrollBarVisibility}" Grid.Column="0" Grid.Row="1" x:Name="PART_HorizontalScrollBar" Style="{DynamicResource SimpleScrollBar}" Orientation="Horizontal" Value="{Binding Path=HorizontalOffset, Mode=OneWay, RelativeSource={RelativeSource TemplatedParent}}" ViewportSize="{TemplateBinding ViewportWidth}" Minimum="0" Maximum="{TemplateBinding ScrollableWidth}" AutomationProperties.AutomationId="HorizontalScrollBar"/>
                                <ScrollBar Visibility="{TemplateBinding ComputedVerticalScrollBarVisibility}" Grid.Column="1" Grid.Row="0" x:Name="PART_VerticalScrollBar" Style="{DynamicResource SimpleScrollBar}" Orientation="Vertical" Value="{Binding Path=VerticalOffset, Mode=OneWay, RelativeSource={RelativeSource TemplatedParent}}" ViewportSize="{TemplateBinding ViewportHeight}" Minimum="0" Maximum="{TemplateBinding ScrollableHeight}" AutomationProperties.AutomationId="VerticalScrollBar"/>
                            </Grid>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>
        </ResourceDictionary>
    </Application.Resources>
</Application>
