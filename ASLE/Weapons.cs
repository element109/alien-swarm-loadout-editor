﻿namespace ASLE
{
    using System.Collections.ObjectModel;
    using System.Windows.Media;

    public class Weapon : Base
    {
        public int UnlockLevel { get; private set; }

        ImageSource _Image;
        public ImageSource Image
        {
            get
            {
                if (Properties.Settings.Default.UnlockLevel < this.UnlockLevel)
                    return MainView.LockedImage;

                return this._Image;
            }
            set { this._Image = value; }
        }

        public Weapon(int level)
        {
            this.UnlockLevel = level;
        }

        public Weapon(int level, string name, Classes @class, ImageSource image)
            : this(level)
        {
            this.Name = name;
            this.Class = @class;
            this.Image = image;
        }
    }

    public class Weapons : ObservableCollection<Weapon>
    {
    }
}
