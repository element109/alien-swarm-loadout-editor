﻿namespace ASLE
{
    using System.Windows;
    using System.Windows.Controls;

    public partial class Loadout : UserControl
    {
        private static readonly DependencyProperty ProfileProperty = DependencyProperty.Register(
                                                                     "Profile",
                                                                     typeof(Profile),
                                                                     typeof(Loadout),
                                                                     new FrameworkPropertyMetadata(null));

        private static readonly DependencyProperty WeaponsProperty = DependencyProperty.Register(
                                                                    "Weapons",
                                                                    typeof(Weapons),
                                                                    typeof(Loadout),
                                                                    new FrameworkPropertyMetadata(null));

        private static readonly DependencyProperty ExtrasProperty = DependencyProperty.Register(
                                                                    "Extras",
                                                                    typeof(Extras),
                                                                    typeof(Loadout),
                                                                    new FrameworkPropertyMetadata(null));

        public Profile Profile
        {
            get { return (Profile)this.GetValue(ProfileProperty); }
            set { this.SetValue(ProfileProperty, value); }
        }

        public Weapons Weapons
        {
            get { return (Weapons)this.GetValue(WeaponsProperty); }
            set { this.SetValue(WeaponsProperty, value); }
        }

        public Extras Extras
        {
            get { return (Extras)this.GetValue(ExtrasProperty); }
            set { this.SetValue(ExtrasProperty, value); }
        }

        public Loadout()
        {
            this.InitializeComponent();
        }
    }
}
