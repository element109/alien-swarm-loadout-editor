﻿namespace ASLE
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Windows.Media;

    public class Profile : Base, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public const string SargeWeapon1 = "asw_default_primary_0";
        public const string SargeWeapon2 = "asw_default_secondary_0";
        public const string SargeExtra = "asw_default_extra_0";

        public const string WildcatWeapon1 = "asw_default_primary_1";
        public const string WildcatWeapon2 = "asw_default_secondary_1";
        public const string WildcatExtra = "asw_default_extra_1";

        public const string FaithWeapon1 = "asw_default_primary_2";
        public const string FaithWeapon2 = "asw_default_secondary_2";
        public const string FaithExtra = "asw_default_extra_2";

        public const string CrashWeapon1 = "asw_default_primary_3";
        public const string CrashWeapon2 = "asw_default_secondary_3";
        public const string CrashExtra = "asw_default_extra_3";

        public const string JaegerWeapon1 = "asw_default_primary_4";
        public const string JaegerWeapon2 = "asw_default_secondary_4";
        public const string JaegerExtra = "asw_default_extra_4";

        public const string WolfeWeapon1 = "asw_default_primary_5";
        public const string WolfeWeapon2 = "asw_default_secondary_5";
        public const string WolfeExtra = "asw_default_extra_5";

        public const string BastilleWeapon1 = "asw_default_primary_6";
        public const string BastilleWeapon2 = "asw_default_secondary_6";
        public const string BastilleExtra = "asw_default_extra_6";

        public const string VegasWeapon1 = "asw_default_primary_7";
        public const string VegasWeapon2 = "asw_default_secondary_7";
        public const string VegasExtra = "asw_default_extra_7";

        public static readonly string[] Commands =
        {
            SargeWeapon1,
            SargeWeapon2,
            SargeExtra,
            WildcatWeapon1,
            WildcatWeapon2,
            WildcatExtra,
            FaithWeapon1,
            FaithWeapon2,
            FaithExtra,
            CrashWeapon1,
            CrashWeapon2,
            CrashExtra,
            JaegerWeapon1,
            JaegerWeapon2,
            JaegerExtra,
            WolfeWeapon1,
            WolfeWeapon2,
            WolfeExtra,
            BastilleWeapon1,
            BastilleWeapon2,
            BastilleExtra,
            VegasWeapon1,
            VegasWeapon2,
            VegasExtra
        };

        private ImageSource _Image;
        public ImageSource Image
        {
            get { return this._Image; }
            set
            {
                this._Image = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Image"));
            }
        }

        private Weapon _WeaponSlot1;
        public Weapon WeaponSlot1
        {
            get { return this._WeaponSlot1; }
            set
            {
                this._WeaponSlot1 = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("WeaponSlot1"));
            }
        }

        private Weapon _WeaponSlot2;
        public Weapon WeaponSlot2
        {
            get { return this._WeaponSlot2; }
            set
            {
                this._WeaponSlot2 = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("WeaponSlot2"));
            }
        }

        private Extra _ExtraSlot;
        public Extra ExtraSlot
        {
            get { return this._ExtraSlot; }
            set
            {
                this._ExtraSlot = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ExtraSlot"));
            }
        }

        private bool _InvalidWeapon1;
        public bool InvalidWeapon1
        {
            get { return this._InvalidWeapon1; }
            set
            {
                this._InvalidWeapon1 = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("InvalidWeapon1"));
            }
        }

        private bool _InvalidWeapon2;
        public bool InvalidWeapon2
        {
            get { return this._InvalidWeapon2; }
            set
            {
                this._InvalidWeapon2 = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("InvalidWeapon2"));
            }
        }

        private bool _InvalidExtra;
        public bool InvalidExtra
        {
            get { return this._InvalidExtra; }
            set
            {
                this._InvalidExtra = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("InvalidExtra"));
            }
        }

        public int Index { get; private set; }

        public Profile(int index)
        {
            this.Index = index;
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }
    }

    public class Profiles : ObservableCollection<Profile>
    {
    }
}