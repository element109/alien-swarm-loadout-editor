﻿namespace ASLE
{
    using System.Windows.Media;

    public enum Classes
    {
        None,
        Officer,
        SpecialWeapons,
        Medic,
        Tech
    }

    public abstract class Base
    {
        public Classes Class { get; set; }
        public string Name { get; set; }
    }
}
