﻿namespace ASLE
{
    using System.Windows;

    public partial class MainWindow : Window
    {
        internal MainWindow(MainView view)
        {
            this.DataContext = view;
            this.InitializeComponent();
            for (int i = 1; i <= view.MaxLevel; i++)
                this.cboLevel.Items.Add(i);
        }
    }
}
