﻿namespace ASLE
{
    using System.Windows.Forms;

    internal static class Message
    {
        public static System.Windows.MessageBoxResult Show(string message, string title, System.Windows.MessageBoxButton buttons, System.Windows.MessageBoxImage image)
        {
            return (System.Windows.MessageBoxResult)MessageBox.Show(message, title, (MessageBoxButtons)buttons, (MessageBoxIcon)image);
        }

        public static System.Windows.MessageBoxResult Show(IWin32Window owner, string message, string title, System.Windows.MessageBoxButton buttons, System.Windows.MessageBoxImage image)
        {
            return (System.Windows.MessageBoxResult)MessageBox.Show(owner, message, title, (MessageBoxButtons)buttons, (MessageBoxIcon)image);
        }
    }
}
