﻿namespace ASLE
{
    using System.Threading;
    using System.Windows;
    using System.Windows.Threading;

    public partial class App : Application
    {
        private Mutex mtx;
        private MainView mainView;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            System.Windows.Forms.Application.EnableVisualStyles();

            bool isFirstInstance;
            this.mtx = new Mutex(true, "A16C2C6D-3D7F-4E75-8440-E04AEA821DC8", out isFirstInstance);
            if (isFirstInstance)
            {
                if (e.Args.Length > 0)
                {
                    switch (e.Args[0].ToLowerInvariant())
                    {
                        case "s":
                        case "-s":
                        case "swarm":
                        case "-swarm":
                            ASLE.Properties.Settings.Default.ReactiveDrop = false;
                            break;
                        case "r":
                        case "-r":
                        case "rd":
                        case "-rd":
                        case "reactivedrop":
                        case "-reactivedrop":
                            ASLE.Properties.Settings.Default.ReactiveDrop = true;
                            break;
                    }
                }

                this.mainView = new MainView();
                this.MainWindow = new MainWindow(this.mainView);
                this.MainWindow.Closing += this.mainView.MainWindow_Closing;
                this.MainWindow.Show();
            }
            else
                this.Shutdown();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            this.mtx.Close();

            if (MainView.Restart)
                System.Windows.Forms.Application.Restart();
        }

        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Message.Show(e.Exception.Message, e.Exception.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            this.Shutdown();
        }
    }
}
