﻿namespace ASLE
{
    using System.Collections.ObjectModel;
    using System.Windows.Media;

    public class Extra : Base
    {
        public int UnlockLevel { get; private set; }

        private ImageSource _Image;
        public ImageSource Image
        {
            get
            {
                if (Properties.Settings.Default.UnlockLevel < this.UnlockLevel)
                    return MainView.LockedImage;

                return this._Image;
            }
            set { this._Image = value; }
        }

        public Extra(int level)
        {
            this.UnlockLevel = level;
        }

        public Extra(int level, string name, Classes @class, ImageSource image)
            : this(level)
        {
            this.Name = name;
            this.Class = @class;
            this.Image = image;
        }
    }

    public class Extras : ObservableCollection<Extra>
    {
    }
}
